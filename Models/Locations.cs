﻿using System;
using System.Collections.Generic;

namespace SurveysApi.Models
{
    public partial class Locations
    {
        public Locations()
        {
            Surveys = new HashSet<Surveys>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime ToDate { get; set; }

        public virtual ICollection<Surveys> Surveys { get; set; }
    }
}
