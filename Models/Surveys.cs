﻿using System;
using System.Collections.Generic;

namespace SurveysApi.Models
{
    public partial class Surveys
    {
        public int Id { get; set; }
        public bool IsComplete { get; set; }
        public int LocationId { get; set; }
        public int VolunteerId { get; set; }
        public bool? DiagnosedDiabetes { get; set; }
        public byte? Age { get; set; }
        public string Gender { get; set; }
        public bool? DiagnosedGestationalDiabetes { get; set; }
        public bool? RelativeWithDiabetes { get; set; }
        public bool? DiagnosedHighBp { get; set; }
        public bool? PhysicallyActive { get; set; }
        public int? Weight { get; set; }
        public byte? HeightFeet { get; set; }
        public byte? HeightInches { get; set; }
        public int? TotalRiskScore { get; set; }
        public byte? Bmi { get; set; }
        public string Race { get; set; }
        public string Ethnicity { get; set; }
        public string CountryOfBirth { get; set; }
        public string ZipCode { get; set; }
        public bool? HaveHealthCoverage { get; set; }
        public string HealthInsurancePlanName { get; set; }
        public bool? ContactForHealthCoverage { get; set; }
        public bool? ContactForDiabetesPrevention { get; set; }
        public string FirstAndLastName { get; set; }
        public string TelephoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public bool? HavePrimaryDoctor { get; set; }
        public bool? FollowUpResults { get; set; }
        public string GlucoseReading { get; set; }
        public string A1cnumber { get; set; }
        public string LipidsCholHdl { get; set; }
        public string LipidsLdlC { get; set; }
        public string LipidsChol { get; set; }
        public string LipidsHdlC { get; set; }
        public string LipidsTrig { get; set; }

        public virtual Locations Location { get; set; }
        public virtual Volunteers Volunteer { get; set; }
    }
}
