﻿using System;
using System.Collections.Generic;

namespace SurveysApi.Models
{
    public partial class Countries
    {
        public int Id { get; set; }
        public string CountryCode { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}
