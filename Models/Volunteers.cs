﻿using System;
using System.Collections.Generic;

namespace SurveysApi.Models
{
    public partial class Volunteers
    {
        public Volunteers()
        {
            Surveys = new HashSet<Surveys>();
        }

        public int Id { get; set; }
        public string PinCode { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Surveys> Surveys { get; set; }
    }
}
