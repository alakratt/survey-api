using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SurveysApi.Data;
using SurveysApi.Models;

namespace SurveysApi.Controllers
{
    [Route("locations")]
    [ApiController]
    public class LocationsController
    {
        private readonly SurveyAppContext _context;

        public LocationsController(SurveyAppContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get a list of locations
        /// </summary>
        /// <returns>List of Locations</returns>
        [HttpGet]
        public async Task<List<Locations>> Read()
        {
            return await _context.Locations.ToListAsync();
        }

        /// <summary>
        /// Gets a list of active survey locations
        /// </summary>
        /// <returns>List of Locations</returns>
        [Route("active")]
        [HttpGet]
        public async Task<List<Locations>> GetActiveLocations()
        {
            return await _context.Locations.Where(loc => DateTime.Compare(loc.ToDate.Date, DateTime.Today.Date) >= 0).ToListAsync();
        }

        /// <summary>
        /// Gets a locations object by its id
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>Locations object</returns>
        [HttpGet("{id}")]
        public async Task<Locations> Read(int id)
        {
            return await _context.Locations.FirstOrDefaultAsync(l => l.Id == id);
        }

        /// <summary>
        /// Gets a list of Locations objects filtered by a search string
        /// </summary>
        /// <param name="searchString">string</param>
        /// <returns>List of Locations objects</returns>
        [Route("search/{searchString}")]
        [HttpGet]
        public async Task<List<Locations>> Read(string searchString)
        {
            return await _context.Locations.Where(loc => loc.Name.Contains(searchString.ToLower())).ToListAsync();
        }

        /// <summary>
        /// Update an existing location in the database
        /// </summary>
        /// <param name="updatedLocation">Locations</param>
        /// <returns>Updated Locations object if successful, if not then null.</returns>
        [HttpPost]
        public async Task<Locations> Update(Locations updatedLocation)
        {
            _context.Locations.Update(updatedLocation);
            int isSuccessful = await _context.SaveChangesAsync();
            return isSuccessful == 1 ? updatedLocation : null;
        }

        /// <summary>
        /// Creates a Locations object and inserts it in the database
        /// </summary>
        /// <param name="newLocation">Locations</param>
        /// <returns>Created Locations object if successful, if not then null.</returns>
        [Route("add")]
        [HttpPost]
        public async Task<Locations> Create(Locations newLocation)
        {
            _context.Locations.Add(newLocation);
            int isSuccessful = await _context.SaveChangesAsync();
            return isSuccessful == 1 ? newLocation : null;
        }

        /// <summary>
        /// Gets a list of Locations with volunteers    
        /// </summary>
        /// <returns>List of Locations</returns>
        [Route("getFilledLocations")]
        [HttpGetAttribute]
        public async Task<List<Locations>> GetFilledLocations()
        {
            IEnumerable<int> lstFilter = await _context.Surveys.GroupBy(s => s.LocationId).Select(g => g.Key).ToListAsync();
            return await _context.Locations.Where(s => lstFilter.Contains(s.Id)).ToListAsync();
         }
    }
}