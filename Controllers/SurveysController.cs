using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SurveysApi.Data;
using SurveysApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace SurveysApi.Controllers
{
    [Route("surveys")]
    [ApiController]
    public class SurveysController
    {
        private readonly SurveyAppContext _context;

        public SurveysController(SurveyAppContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get a list of all surveys
        /// </summary>
        /// <returns>List of Surveys</returns>
        [HttpGet]
        public async Task<List<Surveys>> Read()
        {
            return await _context.Surveys.ToListAsync();
        }

        /// <summary>
        /// Get a survey by its id
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>Surveys object</returns>
        [HttpGet("{id}")]
        public async Task<Surveys> Read(int id)
        {
            return await _context.Surveys.FirstOrDefaultAsync(v => v.Id == id);
        }

        /// <summary>
        /// Get a related Surveys object with Locations and Volunteer data.
        /// </summary>
        /// <param name="volunteerId">int</param>
        /// <param name="locationId">int</param>
        /// <returns>Surveys object</returns>
        [HttpGet("search/{volunteerId}/{locationId}")]
        public async Task<Surveys> Read(int volunteerId, int locationId)
        {
            return await _context.Surveys.Include(vol => vol.Volunteer).Include(loc => loc.Location)
                .Where(v => v.LocationId == locationId).Where(v => v.VolunteerId == volunteerId).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Create a new survey object and insert it to the database
        /// </summary>
        /// <param name="newSurvey">Surveys</param>
        /// <returns>Created Surveys object if successful, if not then null.</returns>
        [HttpPut]
        public async Task<Surveys> Create(Surveys newSurvey)
        {
            newSurvey.TotalRiskScore = CalculateTotalRiskScore(newSurvey);
            _context.Surveys.Add(newSurvey);
            int isSuccessful = await _context.SaveChangesAsync();
            return isSuccessful == 1 ? newSurvey : null;
        }

        /// <summary>
        /// Update an existing survey in the database
        /// </summary>
        /// <param name="updatedSurvey">Surveys</param>
        /// <returns>Updated Surveys object if successful, if not then null.</returns>
        [HttpPost]
        public async Task<Surveys> Update(Surveys updatedSurvey)
        {
            updatedSurvey.TotalRiskScore = CalculateTotalRiskScore(updatedSurvey);
            _context.Surveys.Update(updatedSurvey);
            int isSuccessful = await _context.SaveChangesAsync();
            return isSuccessful == 1 ? updatedSurvey : null;
        }

        private int CalculateTotalRiskScore(Surveys survey)
        {
            int total = 0;
            if (survey.Age.HasValue && survey.Age.Value > 39)
            {
                if (survey.Age.Value < 50)
                    total += 1;
                else if (survey.Age.Value > 49 && survey.Age.Value < 60)
                    total += 2;
                else
                    total += 3;
            }

            total += survey.Gender == "M" ? 1 : 0;
            if (survey.DiagnosedGestationalDiabetes.HasValue && survey.DiagnosedGestationalDiabetes.Value)
                total += 1;

            if (survey.RelativeWithDiabetes.HasValue && survey.RelativeWithDiabetes.Value)
                total += 1;

            if (survey.DiagnosedHighBp.HasValue && survey.DiagnosedHighBp.Value)
                total += 1;

            if (survey.PhysicallyActive.HasValue && survey.PhysicallyActive.Value)
                total += 1;

            if (survey.Weight.HasValue && survey.HeightFeet.HasValue && survey.HeightInches.HasValue)
            {
                int weight = survey.Weight.Value, feet = survey.HeightFeet.Value, inches = survey.HeightInches.Value;

                switch (feet)
                {
                    case 4:
                        switch (inches)
                        {
                            case 10:
                                if (weight >= 119 && weight <= 142)
                                    total += 1;
                                else if (weight >= 143 && weight <= 190)
                                    total += 2;
                                else if (weight > 190)
                                    total += 3;
                                break;
                            case 11:
                                if (weight >= 124 && weight <= 147)
                                    total += 1;
                                else if (weight >= 148 && weight <= 197)
                                    total += 2;
                                else if (weight > 198)
                                    total += 3;
                                break;
                        }
                        break;
                    case 5:
                        switch (inches)
                        {
                            case 0:
                                if (weight >= 128 && weight <= 152)
                                    total += 1;
                                else if (weight >= 153 && weight <= 203)
                                    total += 2;
                                else if (weight > 203)
                                    total += 3;
                                break;
                            case 1:
                                if (weight >= 132 && weight <= 157)
                                    total += 1;
                                else if (weight >= 158 && weight <= 210)
                                    total += 2;
                                else if (weight > 210)
                                    total += 3;
                                break;
                            case 2:
                                if (weight >= 136 && weight <= 163)
                                    total += 1;
                                else if (weight >= 164 && weight <= 217)
                                    total += 2;
                                else if (weight > 217)
                                    total += 3;
                                break;
                            case 3:
                                if (weight >= 141 && weight <= 168)
                                    total += 1;
                                else if (weight >= 169 && weight <= 224)
                                    total += 2;
                                else if (weight > 224)
                                    total += 3;
                                break;
                            case 4:
                                if (weight >= 145 && weight <= 173)
                                    total += 1;
                                else if (weight >= 174 && weight <= 231)
                                    total += 2;
                                else if (weight > 231)
                                    total += 3;
                                break;
                            case 5:
                                if (weight >= 150 && weight <= 179)
                                    total += 1;
                                else if (weight >= 180 && weight <= 239)
                                    total += 2;
                                else if (weight > 239)
                                    total += 3;
                                break;
                            case 6:
                                if (weight >= 155 && weight <= 185)
                                    total += 1;
                                else if (weight >= 186 && weight <= 246)
                                    total += 2;
                                else if (weight > 246)
                                    total += 3;
                                break;
                            case 7:
                                if (weight >= 159 && weight <= 190)
                                    total += 1;
                                else if (weight >= 191 && weight <= 254)
                                    total += 2;
                                else if (weight > 254)
                                    total += 3;
                                break;
                            case 8:
                                if (weight >= 164 && weight <= 196)
                                    total += 1;
                                else if (weight >= 197 && weight <= 261)
                                    total += 2;
                                else if (weight > 261)
                                    total += 3;
                                break;
                            case 9:
                                if (weight >= 169 && weight <= 202)
                                    total += 1;
                                else if (weight >= 203 && weight <= 269)
                                    total += 2;
                                else if (weight > 269)
                                    total += 3;
                                break;
                            case 10:
                                if (weight >= 174 && weight <= 208)
                                    total += 1;
                                else if (weight >= 209 && weight <= 277)
                                    total += 2;
                                else if (weight > 277)
                                    total += 3;
                                break;
                            case 11:
                                if (weight >= 179 && weight <= 214)
                                    total += 1;
                                else if (weight >= 215 && weight <= 285)
                                    total += 2;
                                else if (weight > 285)
                                    total += 3;
                                break;
                        }
                        break;
                    case 6:
                        switch (inches)
                        {
                            case 0:
                                if (weight >= 184 && weight <= 220)
                                    total += 1;
                                else if (weight >= 221 && weight <= 293)
                                    total += 2;
                                else if (weight > 293)
                                    total += 3;
                                break;
                            case 1:
                                if (weight >= 189 && weight <= 226)
                                    total += 1;
                                else if (weight >= 227 && weight <= 301)
                                    total += 2;
                                else if (weight > 301)
                                    total += 3;
                                break;
                            case 2:
                                if (weight >= 194 && weight <= 232)
                                    total += 1;
                                else if (weight >= 233 && weight <= 310)
                                    total += 2;
                                else if (weight > 310)
                                    total += 3;
                                break;
                            case 3:
                                if (weight >= 200 && weight <= 239)
                                    total += 1;
                                else if (weight >= 240 && weight <= 318)
                                    total += 2;
                                else if (weight > 318)
                                    total += 3;
                                break;
                            case 4:
                                if (weight >= 205 && weight <= 245)
                                    total += 1;
                                else if (weight >= 246 && weight <= 327)
                                    total += 2;
                                else if (weight > 327)
                                    total += 3;
                                break;
                        }
                        break;
                }
            }
            return total;
        }

        // private int Triangulate(int maxInches, int initPoints, int weight, int startInches = 0)
        // {
        //     int[] points = new int[] {}
        //     for (int i = startInches; i <= maxInches; i++)
        //     {
        //         for (int w = 100, p = 0; i < weight; w += 5, p++)
        //         {

        //         }
        //     }
        // }

        // private int CalculateBMI(Surveys survey)
        // {
        //     if (survey.Weight.HasValue && survey.HeightFeet.HasValue && survey.HeightInches.HasValue)
        //     {
        //         int weight = survey.Weight.Value, feet = survey.HeightFeet.Value, inches = survey.HeightInches.Value;

        //         switch(feet)
        //         {
        //             case 4:
        //                 for (int i = 0; i <= 11; i++)
        //                 {

        //                 }
        //         }
        //     }
        // }
    }
}