﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using SurveysApi.Models;

namespace SurveysApi.Data
{
    public partial class SurveyAppContext : DbContext
    {
        public SurveyAppContext()
        {
        }

        public SurveyAppContext(DbContextOptions<SurveyAppContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Countries> Countries { get; set; }
        public virtual DbSet<Locations> Locations { get; set; }
        public virtual DbSet<Surveys> Surveys { get; set; }
        public virtual DbSet<Volunteers> Volunteers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            { }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.1-servicing-10028");

            modelBuilder.Entity<Countries>(entity =>
            {
                entity.HasKey(e => e.CountryCode)
                    .HasName("PK_Countries_CountriesCode");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Locations>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ToDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Surveys>(entity =>
            {
                entity.Property(e => e.A1cnumber)
                    .HasColumnName("A1CNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Bmi).HasColumnName("BMI");

                entity.Property(e => e.CountryOfBirth)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DiagnosedHighBp).HasColumnName("DiagnosedHighBP");

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ethnicity)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstAndLastName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.GlucoseReading)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.HealthInsurancePlanName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LipidsChol)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LipidsCholHdl)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LipidsHdlC)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LipidsLdlC)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LipidsTrig)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Race)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TelephoneNumber)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Surveys)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Surveys__Locatio__164452B1");

                entity.HasOne(d => d.Volunteer)
                    .WithMany(p => p.Surveys)
                    .HasForeignKey(d => d.VolunteerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Surveys__Volunte__173876EA");
            });

            modelBuilder.Entity<Volunteers>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PinCode)
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });
        }
    }
}
